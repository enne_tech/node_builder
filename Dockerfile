FROM ubuntu:xenial

RUN apt-get update && \
  apt-get install -y curl gcc g++ make 

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -

RUN apt-get update && \
  apt-get install -y nodejs

RUN npm install -g yarn

ENTRYPOINT ['bash']
